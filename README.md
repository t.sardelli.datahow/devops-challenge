# DevOps Technical Challenge

## Overview
The technical challenge is designed to evaluate the candidate's proficiency in developing a simple backend API and deploying it in Kubernetes. The candidate can use any programming language of their choice. The challenge will involve creating an API endpoint, deploying multiple instances of the microservice, and setting up CI/CD pipelines using GitLab or GitHub Actions.

Candidates will complete the tasks at their own pace. The overall time required to complete the tasks shouldn't be more than a few hours.

## Prerequisites
1. A GitLab/GitHub repository (or any version control system) where the candidate will commit their code.
2. A local Kubernetes cluster.
3. A container registry account for pushing the image (can be the one provided by GitLab/GitHub).

## Tasks

### Section 1: Develop the Backend API

**Objective:** Develop a simple backend API that exposes one endpoint: `GET /me`.

**Tasks:**
1. **Create the API:**
    - Develop a backend API using the programming language of your choice.
    - The API should expose one endpoint: `GET /me`.
    - The endpoint should return the response: `"Hello, my name is <NAME>"`, where `<NAME>` is a configuration value defined at runtime.

2. **Configuration:**
    - Implement a mechanism to set the `<NAME>` value at runtime.

**Deliverables:**
- Source code for the backend API.
- Instructions on how to run the API locally.

### Section 2: Dockerize the Microservice

**Objective:** Create a Dockerfile to containerize the backend API.

**Tasks:**
1. **Write a Dockerfile:**
    - Create a `Dockerfile` that defines how to build a Docker image for the backend API.
    - Ensure the Dockerfile is optimized and includes only the necessary dependencies.

2. **Build and Test the Docker Image:**
    - Build the Docker image locally.
    - Test the Docker image to ensure it runs correctly and the API endpoint works as expected.

**Deliverables:**
- Dockerfile.
- Instructions on how to build and run the Docker image.

### Section 3: CI/CD Pipelines

**Objective:** Create a CI/CD pipeline integrated with the repository to build the container image and push it to a registry.

**Tasks:**
1. **CI/CD Configuration:**
    - Create a GitLab CI configuration file (`.gitlab-ci.yml`) or GitHub Actions workflow file to automate the build and push process.
    - The pipeline should:
        - Build the Docker image.
        - Push the Docker image to a container registry


**Deliverables:**
- Pipelines/Actions configuration to build the image and upload it to the container registry
- Instructions on how to pull the container image.


### Section 4: Kubernetes Cluster

**Objective:** Set up a Kubernetes cluster either locally (Minikube, Kind, K3s, etc.) or using a cloud provider of your choice (AWS, Azure, GCP, etc.)

**Tasks:**
1. **Provision the Cluster:**
    - Provision the Kubernetes cluster in a way that it's easy to reproduce.
    - Verify the cluster is up and running.

**Deliverables:**
- Provisioning scripts or configuration files depending on the technology used.


### Section 5: Microservices Deployment on Kubernetes

**Objective:** Deploy two instances of the microservice in multiple replicas.

**Tasks:**
1. **Kubernetes Manifests:**
    - Write Kubernetes manifests to deploy two instances of the microservice, each one in multiple replicas.
    - The two instances should have two different `<NAME>`s as specified in Section 1
    - Use a tool of your choice to manage the manifests templates.
    - Ensure the services are exposed correctly.
    - Ensure that each instance is deployed in multiple replicas for high availability.

3. **Deploy to Kubernetes:**
    - Apply the Kubernetes manifests to deploy the microservices.
    - Verify the deployments and services are running as expected.

**Deliverables:**
- Kubernetes manifests.
- Instructions on how to invoke the microservices API.

## Submission Instructions

1. **Git Repository:**
    - Push all code, configuration files, and documentation to a GitLab or GitHub repository.
    - Provide the repository URL.

2. **Documentation:**
    - Include a `README.md` file with instructions on how to set up and verify each section.
    - Document any assumptions, decisions, and potential improvements.

3. **Interview Discussion:**
    - Be prepared to discuss your approach, decisions made, and any challenges faced during the follow-up interview.

## Evaluation Criteria

- **Completeness:** All tasks are completed, and deliverables are provided.
- **Code Quality:** Clean, well-structured, and modular code.
- **Documentation:** Clear and comprehensive documentation.
- **Git History:** Clear git history, atomic commits with well defined messages.

Good luck!
